const { workerData, parentPort } = require("worker_threads");

function kvadriraj(niz1, niz2) {
    return niz1.reduce((acc, v, i)=>[...acc, v+niz2[i]], []);
}

parentPort.postMessage(
    kvadriraj(workerData.niz1, workerData.niz2)
);