const { workerData, parentPort } = require("worker_threads");

function generisi(brojElemenata) {
    return Array(brojElemenata).fill(1).map(x => Math.random() * 10)
}

parentPort.postMessage(
    generisi(workerData.brojElemenata)
);